import 'babel-polyfill';
import './assets/styles/global.css';
import '!style-loader!css-loader!Styles/normalizer.css';

import { AppContainer } from 'react-hot-loader';
import React from 'react';
import { render } from 'react-dom';

import Index from 'Pages';


render(
  <AppContainer>
    <Index />
  </AppContainer>,
  document.getElementById('root-entry')
);

// Handle hot reloading requests from Webpack
if (module.hot) {
  module.hot.accept('./pages/index', () => {
    //If we receive a HMR request for our App container, then
    //reload it using require (we can't do this dynamically with import)
    const NextPage = require('./pages/index').default;

    // And render it into the root element again
    render(
      <AppContainer>
        <NextPage />
      </AppContainer>,
      document.getElementById('root-entry')
    );
  });
}
