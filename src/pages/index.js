import React, { Component } from 'react';
import './index.scss';
import type from 'Styles/type.scss';


class Index extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <main className="pages__container">
        <div className="pages__canvas">
          <h1 className={ type.display }>Design Art!</h1>
          <ol>
            <li>go to art museum</li>
            <li>take pictures of art work that you want to design in css</li>
            <li>make css art</li>
          </ol>
        </div>
      </main>
    );
  }
}



export default Index;
