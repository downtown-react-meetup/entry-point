const path = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const { contentBasePath, cssPaths, port, postcssPaths } = require('./common-paths');


module.exports = {
  devServer: {
    https: false,
    host: 'localhost',
    port,

    contentBase: contentBasePath,
    historyApiFallback: true,
    compress: true,

    hot: true,
    inline: true,

    // --progress - [assets, children, chunks, colors, errors, hash, timings, version, warnings]
    stats: {
      assets: true,
      children: true,
      chunks: false,
      colors: true,
      errors: true,
      errorDetails: true, //depends on {errors: true}
      hash: true,
      modules: false,
      publicPath: true,
      reasons: false,
      source: true, //what does this do?
      timings: true,
      version: true,
      warnings: true
    }
  },

  output: {
    filename: '[name].bundle.js',
    publicPath: `http://localhost:${port}/` // Use absolute paths to avoid the way that URLs are resolved by Chrome when they're parsed from a dynamically loaded CSS blob. Note: Only necessary in Dev.
  },

  module: {
    rules: [
      {
        // test: /\.scss$/,
        test: /\.(s*)css$/,
        include: cssPaths,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              // localIdentName: '[folder]__[local]--[hash:base64:10]',
              localIdentName: '[folder]__[local]',
              sourceMap: true,
              importLoaders: 1,
              import: false,
              url: false
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              includePaths: ['assets', 'components', 'pages'],
              outputStyle: 'expanded',
              precision: 5
            }
          }
        ]
      }
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),

    new HtmlWebpackPlugin({
      title: 'css art boxes',
      template: 'index.html',
      inject: true,
      hash: true,
      cache: true,
      showErrors: true,
      chunksSortMode: 'dependency'
    }),
    // Enable multi-pass compilation for enhanced performance
    // in larger projects. Good default.
    new webpack.HotModuleReplacementPlugin({
      multiStep: false
    })
  ]
};
