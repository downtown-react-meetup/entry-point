const path = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const Uglify = require('uglifyjs-webpack-plugin');
const { cssPaths, postcssPaths } = require('./common-paths');


module.exports = {
  entry: {
    vendor: [
      'react-hot-loader', 'react', 'react-dom', 'prop-types', 'babel-polyfill'
    ]
  },
  output: {
    publicPath: './',
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].js'
  },
  recordsPath: path.resolve(__dirname, '..', './recordsPath.json'),

  module: {
    rules: [
      {
        // test: /\.css$/,
        test: /\.(s*)css$/,
        include: cssPaths,
        use: [
          MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                module: true,
                localIdentName: '[folder]__[local]--[hash:base64:10]',
                sourceMap: true,
                importLoaders: 1,
                import: false,
                url: true
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                includePaths: ['assets', 'components', 'pages'],
                outputStyle: 'expanded',
                precision: 5
              }
            }
          ]
      }
    ]
  },

  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),

    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest'],
      minChuncks: Infinity
    }),

    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),

    new HtmlWebpackPlugin({
      title: 'Natour',
      template: 'index.html',
      inject: true,
      hash: true,
      cache: true,
      chunksSortMode: 'dependency',
      showErrors: true
    }),

    new Uglify({
      parallel: true,
      sourceMap: true,
      extractComments: false,
      uglifyOptions: {
        ecma: 6,
        mangle: true,
        output: {
          comments: false,
          beautify: false
        },
        compress: {
          warnings: false,
          drop_console: true,
          drop_debugger: true,
          dead_code: true
        },
        warnings: true
      }
    }),
    new webpack.optimize.AggressiveMergingPlugin()
  ]
};
