const path = require('path');


module.exports = {
  appEntry: './index.js',
  contentBasePath: path.resolve(__dirname, '..', 'src'),
  contextPath: path.resolve(__dirname, '..', 'src'),
  cssPaths: [
    path.resolve(__dirname, '..', 'src/assets/styles'),
    path.resolve(__dirname, '..', 'src/assets/fonts'),
    path.resolve(__dirname, '..', 'src/components'),
    path.resolve(__dirname, '..', 'src/pages'),
    path.resolve(__dirname, '..', 'src/shared')
  ],
  fontPathEntry: [path.resolve(__dirname, '..', 'src/assets/fonts')],
  fontPathOutput: 'assets/fonts/[name].[ext]',
  imagePathEntry: [path.resolve(__dirname, '..', 'src/assets/images')],
  imagePathOutput: 'assets/images/[name].[ext]',
  jsPaths: [
    path.resolve(__dirname, '..', 'src/piperouter'),
    path.resolve(__dirname, '..', 'src/components'),
    path.resolve(__dirname, '..', 'src/configureStore.js'),
    path.resolve(__dirname, '..', 'src/gateway'),
    path.resolve(__dirname, '..', 'src/index.js'),
    path.resolve(__dirname, '..', 'src/lib'),
    path.resolve(__dirname, '..', 'src/logger.js'),
    path.resolve(__dirname, '..', 'src/pages'),
    path.resolve(__dirname, '..', 'src/reducers'),
    path.resolve(__dirname, '..', 'src/pipeline'),
    path.resolve(__dirname, '..', 'src/services'),
    path.resolve(__dirname, '..', 'src/shared')
  ],
  outputPath: path.resolve(__dirname, '..', 'build'),
  port: 8000,
  postcssPaths: path.resolve(__dirname, '..', 'src'),
  resolveAliasPaths: {
    Components: path.resolve(__dirname, '..', 'src/components'),
    Gateway: path.resolve(__dirname, '..', 'src/gateway'),
    Lib: path.resolve(__dirname, '..', 'src/lib'),
    PipeRouter: path.resolve(__dirname, '..', 'src/piperouter'),
    Pages: path.resolve(__dirname, '..', 'src/pages'),
    Reducers: path.resolve(__dirname, '..', 'src/reducers'),
    Services: path.resolve(__dirname, '..', 'src/services'),
    Shared: path.resolve(__dirname, '..', 'src/components/shared'),
    Store: path.resolve(__dirname, '..', 'src/configureStore.js'),
    Styles: path.resolve(__dirname, '..', 'src/assets/styles')
  },
  resolveAliasModules: [
    path.resolve(__dirname, '..', 'src'),
    'node_modules'
  ]
};
